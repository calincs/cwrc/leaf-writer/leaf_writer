# Leaf writer

Integrates the
[javascript leaf writer library](https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer)
with drupal by providing a drupal `leaf_writer` form element and a
`leaf_writer` xml file widget.

## Download the javascript library

### Manual download

Manually download the javascript library from https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer
and put it under `<DRUPAL_ROOT>/library/leafwriter`. Ensure that the content of
the repository is under the `leafwriter` folder.

### Using composer (*recommended*)

Add the snippet below under your drupal project `composer.json` inside the
[repositories](https://getcomposer.org/doc/05-repositories.md) property:
```json
{
  "type": "package",
  "package": [
    {
      "name": "cwrc/leafwriter",
      "version": "1.6.0",
      "type": "drupal-library",
      "extra": {
        "installer-name": "leafwriter"
      },
      "dist": {
        "url": "https://registry.npmjs.org/@cwrc/leafwriter/-/leafwriter-1.6.0.tgz",
        "type": "tar"
      }
    }
  ]
}
```

Then run `composer require cwrc/leafwriter`.

## Installation

1. Install this module as you would do for
[any other drupal module](https://www.drupal.org/docs/extending-drupal/installing-modules#s-step-2-enable-the-module)
2. If you have an existing file field go to (4) below
3. Configure a file field to an entity type or its bundle
4. Ensure that only files with xml as extension can be uploaded to the
configured file
5. Then go to the managed form display of the bundle and select "Leaf writer"
as the widget

## Form API usage

```php
// File has to be of xml extension otherwise an exception would be thrown.
$file = \Drupal\file\Entity\File::load(1);
$form['editor'] = [
  '#type' => 'leaf_writer',
  '#file' => $file,
];
```

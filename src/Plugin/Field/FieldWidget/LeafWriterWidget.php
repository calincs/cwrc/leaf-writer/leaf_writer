<?php

namespace Drupal\leaf_writer\Plugin\Field\FieldWidget;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'leaf_writer' widget.
 *
 * @FieldWidget(
 *   id = "leaf_writer",
 *   label = @Translation("Leaf Writer"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class LeafWriterWidget extends FileWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $file = $items[$delta]->entity ?? NULL;
    if (!$file) {
      return parent::formElement($items, $delta, $element, $form, $form_state);
    }

    $element += [
      '#type' => 'leaf_writer',
      '#file' => $file,
      '#weight' => $delta,
      '#field_name' => $this->fieldDefinition->getName(),
    ];
    return $element;
  }

  /**
   * Checks whether leaf writer widget is applicable.
   */
  protected function leafWriterIsApplicable(FileInterface $file) {
    $supported_mime_types = ['application/xml', 'text/xml'];
    return in_array($file->getMimeType(), $supported_mime_types) && $this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1;
  }

}

<?php

namespace Drupal\leaf_writer\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\file\FileInterface;

/**
 * Provides a form element to edit xml file using leaf writer.
 *
 * @FormElement("leaf_writer")
 */
class LeafWriter extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#file' => NULL,
      '#process' => [
        [$class, 'processFile'],
      ],
      '#element_validate' => [
        [$class, 'validateXmlInput'],
      ],
      '#base_type' => 'textarea',
      '#theme_wrappers' => ['text_format_wrapper'],
    ];
  }

  /**
   * Render API callback to process the file using leaf writer.
   */
  public static function processFile(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $file = $element['#file'];
    $supported_mime_types = ['application/xml', 'text/xml'];
    if (!$file || !($file instanceof FileInterface) ||  !in_array($file->getMimeType(), $supported_mime_types)) {
      throw new \InvalidArgumentException('Invalid file argument provided for #file parameter.');
    }

    $user = static::currentUser();

    // Ensure that children appear as subkeys of this element.
    $element['#tree'] = TRUE;
    $keys_not_to_copy = [
      // Make \Drupal::formBuilder()->doBuildForm() regenerate child properties.
      '#parents',
      '#id',
      '#name',
      // Do not copy this #process function to prevent
      // \Drupal::formBuilder()->doBuildForm() from recursing infinitely.
      '#process',
      // Ensure #pre_render functions will be run.
      '#pre_render',
      // Description is handled by theme_text_format_wrapper().
      '#description',
      // Ensure proper ordering of children.
      '#weight',
      // Properties already processed for the parent element.
      '#prefix',
      '#suffix',
      '#attached',
      '#processed',
      '#theme_wrappers',
      '#file',
      // Required flag for the textarea is handled by the element
      // validate callback.
      '#required',
      '#value',
      '#default_value',
    ];
    // Move this element into sub-element 'value'.
    unset($element['value']);
    foreach (Element::properties($element) as $key) {
      if (!in_array($key, $keys_not_to_copy)) {
        $element['value'][$key] = $element[$key];
      }
    }

    // Set the leaf writer data attributes.
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $uri = $file->getFileUri();
    $full_path = \Drupal::service('file_url_generator')->generateString($uri);
    $element['#attributes']['data-leaf-writer-config'] = Json::encode([
      'xml_file_url' => $host . $full_path,
      'user' => [
        'name' => $user->getDisplayName(),
        'id' => $user->id(),
      ],
    ]);

    $element['value']['#type'] = $element['#base_type'];
    $element['value'] += static::elementInfo()->getInfo($element['#base_type']);
    $element['value']['#value'] = file_get_contents($uri);
    $element['value']['#required'] = !empty($element['#required']);
    $element['value']['#attributes']['style'] = 'visibility: hidden;display: none;';
    $element['value']['#attributes']['class'][] = 'leaf-writer--xml-input';

    $element['container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['leaf-writer--container'],
        // @todo find a way to do not hard code this id.
        'id' => 'leaf-writer-container',
      ],
    ];

    // Required when retrieving the file we are dealing with.
    $element['fids'] = [
      '#type' => 'hidden',
      '#value' => [$file->id()],
    ];

    $element['#attached']['library'][] = 'leaf_writer/initializer';
    return $element;
  }

  /**
   * Render API callback: Validates the leaf_writer element.
   */
  public static function validateXmlInput(&$element, FormStateInterface $form_state, &$complete_form) {
    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
    if (is_string($input)) {
      $xml = $input;
    }
    else {
      $xml = $input['value'] ?? '';
    }

    if ($input_exists && !empty($element['#required']) && !$xml) {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
    if ($xml && !static::isValidXml($xml)) {
      $form_state->setError($element, t('@name has an invalid xml string.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if (!empty($input['value'])) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $element['#file'];
      @file_put_contents($file->getFileUri(), $input['value']);
    }
    return explode(' ', $input['fids']);
  }

  /**
   * Callback helper method to validate if the xml string value is valid.
   *
   * @param string $value
   *   The xml value.
   *
   * @return bool
   *   TRUE when xml is valid, FALSE otherwise.
   */
  protected static function isValidXml(string $value) {
    $prev = libxml_use_internal_errors(true);

    $doc = simplexml_load_string($value);
    $errors = libxml_get_errors();

    libxml_clear_errors();
    libxml_use_internal_errors($prev);

    return FALSE !== $doc && empty($errors);
  }

  /**
   * Wraps the current user.
   *
   * \Drupal\Core\Session\AccountInterface
   */
  protected static function currentUser() {
    return \Drupal::currentUser();
  }

  /**
   * Wraps the element info service.
   *
   * @return \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected static function elementInfo() {
    return \Drupal::service('element_info');
  }

}
